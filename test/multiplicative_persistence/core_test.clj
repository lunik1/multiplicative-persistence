(ns multiplicative-persistence.core-test
  (:require [clojure.test :refer :all]
            [multiplicative-persistence.core :refer :all]
            [clojure.math.numeric-tower :as math]))

(deftest number-length-test
  (is (= 1 (number-length 1)))
  (is (= 1 (number-length 0)))
  (is (= 10 (number-length 1234567890)))
  (is (= 10 (number-length -1234567890)))
  (is (= 10 (number-length 2r1010011010 2)))
  (is (= 3 (number-length 0771 8)))
  (is (= 8 (number-length 0xdeadbeef 16)))
  (is (= 2 (number-length 13r42 13)))
  (is (= 7 (number-length 36rHUNTER2 36))))

(deftest digits-test
  (is (= '(1) (digits 1)))
  (is (= '(0) (digits 0)))
  (is (= '(1 2 3) (digits 123)))
  (is (= '(0xd 0xe 0xa 0xd 0xb 0xe 0xe 0xf) (digits 0xdeadbeef 16))))

(deftest multiply-digits-test
  (is (= 1 (multiply-digits 1)))
  (is (= 0 (multiply-digits 0)))
  (is (= 6 (multiply-digits 123)))
  (is (= (math/expt 15 4) (multiply-digits 0xffff 16))))

(deftest multiplicative-persistence-seq-test
  (is (= '(1) (multiplicative-persistence-seq 1)))
  (is (= '(0) (multiplicative-persistence-seq 0)))
  (is (= '(277777788888899 4996238671872 438939648 4478976 338688 27648 2688 768 336 54 20 0) (multiplicative-persistence-seq 277777788888899))))
