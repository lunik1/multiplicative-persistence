(ns multiplicative-persistence.core
  (:require [clojure.math.numeric-tower :as math]
            [medley.core :as medley]
            [clojure.math.combinatorics :as comb])
  (:gen-class))

(defn log
  "Return the log of x base b"
  [x b]
  (/ (Math/log x) (Math/log b)))

(defn number-length
  "Return the number of digits in integer n in base b. b is 10 if not specified."
  ([n] (number-length n 10))
  ([n b] (if (= n 0)
           1
           (-> n math/abs (log b) int inc))))

(defn digits
  "Return the digits of n in base b. b is 10 if not specified."
  ([n] (digits n 10))
  ([n b] (for [x (range (dec (number-length n b)) -1 -1)] (mod (quot n (math/expt b x)) b))))

(defn ||
  "Concatenates positive integers. (||) returns 0. Works in base 10."
  ([] 0)
  ([x] (cast Number x))
  ([x y] (+ (* x (math/expt 10 (inc (int (log y 10))))) y))
  ([x y & more]
   (reduce || (|| x y) more)))

(defn ||coll
  "Concantenates collections of positive integers. A base b can be specified, defaults to 10"
  ([coll] (||coll coll 10))
  ([coll b] (reduce #(+ (* %1 (math/expt b (inc (int (log %2 b))))) %2) coll)))

(defn multiply-digits
  "Return the result of multiplying the digits of n together. An optional base b can be specified, defaults to 10"
  ([n] (multiply-digits n 10))
  ([n b] (apply * (digits n b))))

(defn multiplicative-persistence-seq
  "Return the sequence of numbers obtained from n when finding the mupltiplicative persitence in base b. b defaults to 10."
  ([n] (multiplicative-persistence-seq n 10))
  ([n b] (medley/take-upto (partial > b) (iterate #(multiply-digits %1 b) n))))

(defn mdr
  "Return the multplicative digital root of n in base b. b is 10 if not specified."
  ([n] (mdr n 10))
  ([n b] (last (multiplicative-persistence-seq n b))))

(defn n-digit-combinations-no-zero-or-one
  "Return a lazy-seq of all the possible combinations of n digits excluding zeroes and ones in base b.
  b defaults to 10"
  ([n] n-digit-combinations-no-zero-or-one n 10)
  ([n b] (comb/combinations (apply concat (for [x (range 2 b)] (repeat n x))) n)))

(defn max-key-first
  "Returns the x for which (k x), a number, is greatest.
  If there are multiple such xs, the first one is returned."
  ([k x] x)
  ([k x y] (if (>= (k x) (k y)) x y))
  ([k x y & more]
   (let [kx (k x) ky (k y)
         [v kv] (if (>= kx ky) [x kx] [y ky])]
     (loop [v v kv kv more more]
       (if more
         (let [w (first more)
               kw (k w)]
           (if (> kw kv
(defn find-greatest-persistence
  "Finds the smallest number with x digits with the greatest multplicative persitence in base b.
  b defaults to 10"
  ([x] (find-greatest-persistence x 10))
  ([x b] (first (apply max-key-first count (map
                                   (comp #(multiplicative-persistence-seq %1 b) #(||coll %1 b))
                                   (apply concat (for [i (range 1 (inc x))] (n-digit-combinations-no-zero-or-one i b))))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  ;; (run! println (map #(multiplicative-persistence-seq %1 31) (range 1000000))))
  (println (apply max-key count (map #(multiplicative-persistence-seq %1 16) (range 1000000)))))
